# Qualifiers for ARIAC

The following qualification tasks have been released:

1. [Qualification task 1 (closed)](https://bitbucket.org/osrf/ariac/wiki/qualifiers/qual1)
1. [Qualification task 2 (closed)](https://bitbucket.org/osrf/ariac/wiki/qualifiers/qual2)
1. [Qualification task 3 (closed)](https://bitbucket.org/osrf/ariac/wiki/qualifiers/qual3)
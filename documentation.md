# Documentation for Gazebo Environment for Agile Robotics (GEAR)


GEAR is the software used by teams participating in the Agile Robotics for Industrial Automation Competition (ARIAC) hosted by the National Institute of Standards and Technology (NIST).

1. [Competition Specifications](https://bitbucket.org/osrf/ariac/wiki/competition_specifications)
    * The specifications of the ARIAC competition.
        * Competition scenarios.
        * Sensors and arms available.
    * [Scoring Metrics](https://bitbucket.org/osrf/ariac/wiki/scoring)
    * [Details of Parts Specifications](https://bitbucket.org/osrf/ariac/wiki/frame_specifications)
1. [Finals](https://bitbucket.org/osrf/ariac/wiki/finals)
    * Details of how the Finals will be run and how submissions will work.
    * [Finals specifications.](https://bitbucket.org/osrf/ariac/wiki/finals_specs)
1. [System Requirements](https://bitbucket.org/osrf/ariac/wiki/system_requirements)
    * The system requirements for GEAR.
1. [Competition Interface](https://bitbucket.org/osrf/ariac/wiki/competition_interface_documentation)
    * How contestants are to communicate with GEAR.
1. [Environment Configuration Specifications](https://bitbucket.org/osrf/ariac/wiki/configuration_spec)
    * How to configure the GEAR competition environment.
        * How to add sensors.
        * How to select and place the arm.
1. [Logging and Playback for ARIAC Qualifiers](https://bitbucket.org/osrf/ariac/wiki/qualifiers/logging)
1. [ARIAC Update Policy](https://bitbucket.org/osrf/ariac/wiki/update_policy)